<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# kivy_dyn_chi 0.3.11

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_dyn_chi/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_dyn_chi)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_dyn_chi/release0.3.10?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_dyn_chi/-/tree/release0.3.10)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_dyn_chi)](
    https://pypi.org/project/ae-kivy-dyn-chi/#history)

>ae_kivy_dyn_chi module 0.3.11.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_dyn_chi/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_dyn_chi/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_dyn_chi/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_dyn_chi/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_dyn_chi/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_dyn_chi/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_dyn_chi)](
    https://gitlab.com/ae-group/ae_kivy_dyn_chi/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_dyn_chi)](
    https://gitlab.com/ae-group/ae_kivy_dyn_chi/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_dyn_chi)](
    https://gitlab.com/ae-group/ae_kivy_dyn_chi/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_dyn_chi)](
    https://pypi.org/project/ae-kivy-dyn-chi/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_dyn_chi)](
    https://gitlab.com/ae-group/ae_kivy_dyn_chi/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_dyn_chi)](
    https://libraries.io/pypi/ae-kivy-dyn-chi)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_dyn_chi)](
    https://pypi.org/project/ae-kivy-dyn-chi/#files)


## installation


execute the following command to install the
ae.kivy_dyn_chi module
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-dyn-chi
```

if you want to contribute to this portion then first fork
[the ae_kivy_dyn_chi repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_dyn_chi "ae.kivy_dyn_chi code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_dyn_chi):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_dyn_chi/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_dyn_chi.html
"ae_kivy_dyn_chi documentation").
